package pe.edu.tecsup.vigym_f

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity_Registro : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_registro)

        val registro = findViewById<Button>(R.id.registro)
        registro.setOnClickListener {
            val log= Intent(this, MainActivity::class.java)
            startActivity(log)}



    }
}