package pe.edu.tecsup.vigym_f

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.Theme_ViGym_F)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val login = findViewById<Button>(R.id.ingresar)
        login.setOnClickListener {
            val Inicio= Intent(this, MainActivity_Inicio::class.java)
            startActivity(Inicio)}

        val registrar = findViewById<Button>(R.id.registrarse)
        registrar.setOnClickListener {
            val regis= Intent(this, MainActivity_Registro::class.java)
            startActivity(regis)}



    }
}